import './app.css'

import CardList from './components/CardList'
import FilterStatus from './components/FilterStatus'

import { GlobalProvider } from './context/GlobalState'

function App() {
  return (
    <GlobalProvider>
      <FilterStatus />
      <CardList />
    </GlobalProvider>
  );
}

export default App;
