import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles({
    cardContent: {
        flexGrow: 1
    },
    media: {
        paddingTop: '56.25%'
    },
    root: {
        maxWidth: 345,
        margin: 5
    },
})

const CardItem = ({ character }) => {
    const classes = useStyles()
    return (
        <>
        <Card className={classes.root}>
            <CardMedia 
             className={classes.media}
             image={character.image}
            />
            <CardContent className={classes.grow}>
                <Typography>Name: {character.name}</Typography>
                <Typography>Status: {character.status}</Typography>
                {/* <Typography>Species: {character.species}</Typography>
                <Typography>Gender: {character.gender}</Typography>
                <Typography>Location: {character.location.name}</Typography> */}
            </CardContent>
        </Card>
        </>
    )
}

export default CardItem
