import React, { useContext } from 'react'


import { GlobalContext } from '../context/GlobalState'


const FilterStatus = () => {
    const { characters, filteredStatus } = useContext(GlobalContext)
    const cloneChars = characters.map(char => char.status)
    
    

  const updateFilter = (charStatus) => {
    console.log(filteredStatus(charStatus))
  }
    
    
    return (
        <>
          <select name='status'>
            <option value='Show All'>All</option>
            <option value='alive'>Alive</option>
            <option value='dead'>Dead</option>
          </select>
        </>
    )
}

export default FilterStatus
