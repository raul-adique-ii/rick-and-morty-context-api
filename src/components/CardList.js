import React, { useContext, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import CardItem from './CardItem'

import { GlobalContext } from '../context/GlobalState';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    }
}))

const CardList = () => {
    const { characters, getAllCharacters } = useContext(GlobalContext)
    const classes = useStyles()

    useEffect(() => {
        getAllCharacters()
    }, [])
    return (
        <Grid container className={classes.root} spacing={4}>
            {characters.map((character) => (
            <Grid item key={character.id} xs={12} sm={6} md={4}>
                <CardItem character={character}/>
            </Grid>
            ))}
        </Grid>
    )
}

export default CardList
