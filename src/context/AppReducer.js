export default (state, action) => {
    switch(action.type) {
        case 'GET_ALLCHARACTERS':
            return {
                ...state,
                loading: false,
                characters: action.payload
            }
        case 'CHARACTERS_ERROR':
            return {
                ...state,
                error: action.payload
            }
        case 'FILTERED_STATUS':
            return {
                ...state,
                characters: state.characters.filter(char => char.status === action.payload)
            }
        default:
            return state
    }
}