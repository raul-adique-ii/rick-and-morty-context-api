import React, { createContext, useReducer } from 'react'
import axios from 'axios'

import AppReducer from './AppReducer'

// Initial State
const initialState = {
    characters: [],
    error: null,
    loading: true
}

// Create context
export const GlobalContext = createContext(initialState)

// Provider
export const GlobalProvider = ({ children }) => {
    const [ state, dispatch] = useReducer(AppReducer, initialState)

    // ACTIONS
    const getAllCharacters = async () => {
        try {
            const response = await axios.get('https://rickandmortyapi.com/api/character')
            dispatch({
                type: 'GET_ALLCHARACTERS',
                payload: response.data.results
            })
        } catch (error) {
            dispatch({
                type: 'CHARACTERS_ERROR',
                payload: error.response.data.error
            })
        }
    }

    const filteredStatus = (status) => {
        dispatch({
            type: 'FILTERED_STATUS',
            payload: status
        })
    }

    return (
        <GlobalContext.Provider value={{
            characters: state.characters,
            error: state.error,
            loading: state.loading,
            getAllCharacters,
            filteredStatus
        }}>
            { children }
        </GlobalContext.Provider>
    )
}